package main

import (
	"bufio"
	"errors"
	"fmt"
	"net"
	"net/textproto"
	"strings"
)

// Bot is a bot that operates in a single channel.
type Bot struct {
	server  string
	port    string
	channel string
	nick    string
	pass    string
	conn    net.Conn
}

// Message describes the structure of an IRC message. Has an optional prefix,
// the command, the trailing argument, and separately, the rest of the
// arguments.
type Message struct {
	prefix    string
	command   string
	arguments []string
	trailing  string
}

func NewBot() *Bot {
	return &Bot{
		server:  "irc.freenode.net",
		port:    "6667",
		nick:    "meowth",
		channel: "#bestfriendsclub",
		pass:    "",
		conn:    nil,
	}
}

func (bot *Bot) Connect() net.Conn {
	conn, err := net.Dial("tcp", bot.server+":"+bot.port)

	if err != nil {
		fmt.Println("Unable to connect :( ", err)
	}

	bot.conn = conn

	fmt.Printf("Connected to IRC server %s (%s)\n", bot.server,
		bot.conn.RemoteAddr())

	return bot.conn
}

// ParseMessage takes a raw string from the irc server and parses it.
func ParseMessage(raw string) (Message, error) {
	var message Message
	var split []string

	// Pull out the optional prefix.
	if strings.HasPrefix(raw, ":") {
		split = strings.SplitN(raw, " ", 2)
		message.prefix = split[0]
		raw = split[1]
	}

	// Get the traling argument.
	split = strings.SplitN(raw, ":", 2)

	if len(split) != 2 {
		return message, errors.New("Command could not be parsed.")
	}
	message.trailing = split[1]
	raw = split[0]

	// Get the command and remaining arguments.
	split = strings.SplitN(raw, " ", 2)
	message.command = split[0]
	if len(split) > 1 {
		message.arguments = strings.Fields(split[1])
	}

	return message, nil
}

// HandleMessage deals with pings and being messaged.
func HandleMessage(raw string, c chan string) {
	message, err := ParseMessage(raw)

	if err != nil {
		return
	}

	switch message.command {
	case "PING":
		c <- "PONG :" + message.trailing
	case "PRIVMSG":
		correction := Correct(message.trailing)
		if correction != "" {
			c <- "PRIVMSG " + message.arguments[0] + " :I think you mean " + correction + "*"
		}
	}
}

func (bot *Bot) SendMessages(c chan string) {
	for {
		fmt.Fprintln(bot.conn, <-c)
	}
}

func main() {
	ircbot := NewBot()
	conn := ircbot.Connect()

	fmt.Fprintf(conn, "USER %s 8 * :%s\n", ircbot.nick, ircbot.nick)
	fmt.Fprintf(conn, "NICK %s\n", ircbot.nick)
	fmt.Fprintf(conn, "JOIN %s\n", ircbot.channel)

	defer conn.Close()
	reader := bufio.NewReader(conn)
	tp := textproto.NewReader(reader)

	c := make(chan string, 10)
	go ircbot.SendMessages(c)

	for {
		line, err := tp.ReadLine()
		if err != nil {
			break
		}

		fmt.Println(line)
		go HandleMessage(line, c)
	}
}
