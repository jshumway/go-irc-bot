package main

import (
	"strings"
)

// "Correct" all occurances of certain often misused words to other forms. This
// is done randomly. It does not analyze grammer.
func Correct(message string) string {
	corrections := map[string]string{
		"youre":   "your",
		"your":    "you're",
		"you're":  "your",
		"their":   "they're",
		"they're": "there",
		"there":   "their",
		"to":      "too",
		"too":     "two",
		"two":	   "to",
	}

	words := strings.Fields(message)

	for i := 0; i < len(words); i++ {
		nword, ok := corrections[strings.ToLower(words[i])]

		if ok {
			return nword
		}
	}

	return ""
}
